# A dictionary of movie critics and their ratings of a small
# set of movies
critics={'Lisa Rose': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.5, 'Just My Luck': 3.0, 'Superman Returns': 3.5, 'You, Me and Dupree': 2.5, 'The Night Listener': 3.0},'Gene Seymour': {'Lady in the Water': 3.0, 'Snakes on a Plane': 3.5,'Just My Luck': 1.5, 'Superman Returns': 5.0, 'The Night Listener': 3.0,
'You, Me and Dupree': 3.5},'Michael Phillips': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.0,'Superman Returns': 3.5, 'The Night Listener': 4.0},
'Claudia Puig': {'Snakes on a Plane': 3.5, 'Just My Luck': 3.0,'The Night Listener': 4.5, 'Superman Returns': 4.0,
'You, Me and Dupree': 2.5},'Mick LaSalle': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0,'Just My Luck': 2.0, 'Superman Returns': 3.0, 'The Night Listener': 3.0,
'You, Me and Dupree': 2.0},'Jack Matthews': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0,'The Night Listener': 3.0, 'Superman Returns': 5.0, 'You, Me and Dupree': 3.5},
'Toby': {'Snakes on a Plane':4.5,'You, Me and Dupree':1.0,'Superman Returns':4.0}}

# Find the similarity score between items
import math
def findSimilarity(prefs,person1,person2):
    sItems={}
    for item in prefs[person1]:
        if(item in prefs[person2]):
            sItems[item]=1
    if(len(sItems)==0):
        return 0
    sumOfSquares=sum([pow( prefs[person1][item]-prefs[person2][item],2) for item in sItems])
    return 1/(1+sumOfSquares)
#<<<<<<< HEAD
def findSimPearson(prefs,person1,person2):
    sItems={}
    #filter the items
    for item in prefs[person1]:
        if(item in prefs[person2]):
            sItems[item]=1
    if(len(sItems)==0):
        return 0
    #find sums
    n=len(sItems)
    sumP1=sum([prefs[person1][x] for x in sItems ])
    sumP2=sum([prefs[person2][x] for x in sItems ])
    print('{} {} '.format(sumP1,sumP2))
    #find sum of multipliers
    sumOfProducts=sum([prefs[person1][x]*prefs[person2][x] for x in sItems])
    #sum of squares
    print(sumOfProducts)
    sumOfSquares1=sum([prefs[person1][x]**2 for x in sItems])
    sumOfSquares2=sum([prefs[person2][x]**2 for x in sItems])
    print('{} {}'.format(sumOfSquares1,sumOfSquares2))
    #find the pearson correlation
    denominator=math.sqrt((sumOfSquares1-pow( sumP1,2)/n)*(sumOfSquares2-pow(sumP2,2)/n))
    numerator=sumOfProducts-sumP1*sumP2/n
    print('{} {}'.format(denominator,numerator))
    if(denominator==0):
        return 0
    else:
        return numerator/denominator
    
    
#=======
def pearsonSimilarity(prefs,person1,person2):
    sItems={}
    for item in prefs[person1]:
        if item in prefs[person2]:
            sItems[item]=1
    if(len(sItems)==0):
        return 0
    #find the sums
    sum1=sum([prefs[person1][x] for x in sItems])
    sum2=sum([prefs[person2][x] for x in sItems])
    #find sum of products
    sumOfP=sum([prefs[person1][x]* prefs[person2][x] for x in sItems])
    #find sum of squares
    sumOfSq1=sum([prefs[person1][x]**2 for x in sItems])
    sumOfSq2=sum([prefs[person2][x]**2 for x in sItems])
    n=len(sItems)
    numerator=sumOfP-sum1*sum2/len(sItems)
    denominator=math.sqrt((sumOfSq1-sum1/n)*(sumOfSq2-sum2/n))
    if(denominator==0):
        return 0
    return numerator/denominator

#>>>>>>> 6ec2f9b7b5a45b8654d07a3def36376504cf3faa
